import React from 'react';

class ManageHappiness extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      happinessInput: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ happinessInput: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        Pokemon's Happiness
        {' '}
        <input type="text" name="input" onChange={this.handleChange}/>
        {' '}
        <input type="submit" value="Save happiness"/>
      </form>
    );
  }
}

export default ManageHappiness;
